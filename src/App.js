import React from 'react';
import Login from './components/adminLogin';
import './App.css';
import {BrowserRouter as Router, Routes, Route} from 'react-router-dom';
// import Reset from './components/Reset';
import Dashboard from './components/Dashboard';
import Reset from './components/Reset';


function App() {
  return (
    <Router>
      <div className="App">
        
        <Routes>
          <Route path='/' element = {<Login/>}/>
          <Route path='/reset' element = {<Reset/>} />
          <Route path='/dashboard'element = {<Dashboard/>} />
        </Routes>
      </div>
    </Router>
  );
}

export default App;
