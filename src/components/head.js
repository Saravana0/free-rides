import React from 'react';
import './head.css';
import Car from './vectorImages/Car.png';
import Notification from './vectorImages/Notification.png';

function Header () {
    return (
        <div className='header-container'>
            <div className='head-sub'>
                <div className='logo-container'>
                    <div><img src = {Car} alt ='logo'/></div>
                    <div><h2> Free Rides </h2></div>
                </div>
                <div className='notify'>
                    <img src ={Notification} alt= 'notification'/>
                </div>
            </div>
        </div>
    );
}

export default Header;