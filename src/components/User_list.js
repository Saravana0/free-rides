import React from 'react';
import './User_list.css';
import Table from './Table';

function User_list() {
    return (
        <div className='user-container'>
            <p className='user-text'> Users </p>
            <div className='table-container'>
                <Table />
            </div>
        </div>
    );
}

export default User_list;