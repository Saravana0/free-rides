import React from 'react';
import Header from './head';
import Navbar from './navbar';
import User_list from './User_list';
import './Dashboard.css';

function Dashboard () {
    return (
        <div className='overall-container'>
           <Header />
           <div className='two-component'>
                <Navbar />
                <User_list />
           </div>
        </div>
    );
};

export default Dashboard;