import React from 'react';
import './Reset.css';
import Car from './vectorImages/Car.png';
import Lockpassword from './vectorImages/Lockpassword.png';
import {Link} from 'react-router-dom'; 

function Reset() {
    return (
      <div className='reset-container'>
           <h1 className="header"><img src={Car} alt="car"></img>  Free Rides</h1>
          <section>
            <div className="card">
              <div className="card-title">Reset Password</div><br></br><br></br>
                <div className="card-content">
                    <form>
                        <div className="reset">
                        <input type="text "className="text" id="password"  placeholder="Enter Your Password" required="required"></input>
                        <img src={Lockpassword} alt="car"></img>
                        </div>
                        
                      <br></br>
                        <button type="submit" className="set-btn" id="submit">Save Password</button><br></br>
                        <Link to ="/" className='back'>back to login</Link>

                    </form> 

                </div>

            </div>
          </section>
          
      </div>
    );
  }
  
  export default Reset;