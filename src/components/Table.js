import React from 'react';
import './Table.css';

function Table() {
    return (
        <div className='full-sub-container'>
            <p className='user-list-heading'>User Lists</p>
            <div className='sub-table'>
                <table>
                    <tr>
                        <th>Name</th>
                        <th>Mobile Number</th>
                        <th>Email-Id</th>
                        <th>Users</th>
                    </tr>
                    <tr>
                        <td>Anom</td>
                        <td>1956789904</td>
                        <td>Murugan@gmail.com</td>
                        <td>
                        <label class="switch">
                            <input type="checkbox"/>
                            <span class="slider round"></span>
                        </label>
                        </td>
                    </tr>
                    <tr>
                        <td>Megha</td>
                        <td>191956789904</td>
                        <td>Raja@gmail.com</td>
                        <td>
                        <label class="switch">
                            <input type="checkbox"/>
                            <span class="slider round"></span>
                        </label>
                        </td>
                    </tr>
                    <tr>
                        <td>Subham</td>
                        <td>1956789904</td>
                        <td>kala@gmail.com</td>
                        <td>
                        <label class="switch">
                            <input type="checkbox"/>
                            <span class="slider round"></span>
                        </label>
                        </td>
                    </tr>
                    <tr>
                        <td>Subham</td>
                        <td>1956789904</td>
                        <td>kala@gmail.com</td>
                        <td>
                        <label class="switch">
                            <input type="checkbox"/>
                            <span class="slider round"></span>
                        </label>
                        </td>
                    </tr>
                    <tr>
                        <td>Megha</td>
                        <td>191956789904</td>
                        <td>Raja@gmail.com</td>
                        <td>
                        <label class="switch">
                            <input type="checkbox"/>
                            <span class="slider round"></span>
                        </label>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    );
};

export default Table;