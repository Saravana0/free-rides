import React from 'react';
import Car from './vectorImages/Car.png';
import Username from './vectorImages/Username.png';
import Lockpassword from './vectorImages/Lockpassword.png';
import {Link} from 'react-router-dom'; 
import './adminLogin.css';
// import Dashboard from './Dashboard';



function Login() {
    return (
        <div className='container'>
            <div className='logo'>
                <img src={Car} alt="Logo" />
                <span className='gradient'><h1> Free Rides </h1></span>
            </div>
            <div className='form-container'>
                <div className='element-container'>
                    <h2> Admin Login </h2>
                    <form>
                        <div className='sub-container'>
                            <div className="user">
                                <img src={Username} alt="Username" />
                                <input type="text" name="name" placeholder='Username' />
                            </div>
                            <div className="password">
                                <img src={ Lockpassword} alt="password" />
                                <input type="text" name="name" placeholder='Password'/>
                            </div> 
                            <Link to='/reset' className='forgot-click'><p> Forgot Password  ? </p></Link>
                        </div>
                        <div className='sub-btn'>
                            {/* <input type="submit" value="Log in"  onSubmit={< Dashboard/>}/> */}
                            <Link to="/dashboard"  className='login-btn'>
                                <button>
                                    Login
                                </button>
                            </Link>
                        </div>
                    </form>
                </div>
                
                
            </div>
        </div>
    )
}

export default Login;