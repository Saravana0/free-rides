import React from 'react';
import './navbar.css';
import Home from './vectorImages/Home.png';
import Useractive from './vectorImages/Useractive.png';
import News from './vectorImages/News.png';
import Chat from './vectorImages/Chat.png';
function Navbar () {
    return (
        <div className='navbar-container'>
            <div className='menu-container'>
                <div className='dash-btn'>
                    <img src = {Home} alt='home-button'/>
                   <div><p> Dashboard </p></div>
                </div>
                <div className='user-btn'>
                    <img src ={Useractive} alt='useractive-button' />
                    <div><p> Users </p></div>
                </div>
                <div className='news-btn'>
                    <img src = {News} alt='news-btn'/>
                    <div><p> News Feed </p></div>
                </div>
                <div className='chat-btn'>
                    <img src ={Chat} alt='Chat.png'/>
                    <div><p> chat </p></div>
                </div>
            </div>
        </div>
    );
}

export default Navbar;